<?php

/**
 * @file
 * The gdriving-block theme
 *
 * The gdriving-block theme allows users to customize the gmap display
 * @see template_preprocess_gdriving_block()
 */
?>
<div class="gDirections">
	<?php echo $gmap; ?>
	<div class="directionsExtended"></div>
</div>